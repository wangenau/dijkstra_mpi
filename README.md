# dijkstra_mpi

C implementation of Dijkstra's algorithm to find the shortest path in a graph from a starting node to every other node.
The code has been parallelized using OpenMPI.

## Compilation

An MPI compiler (mpicc) is needed to compile the program. A makefile is provided with the code. To compile it simply type into your terminal:

```bash
$ make
```

## Usage

```bash
$ mpirun -np PROC ./dijkstra [NODE] [DIM]
```

* PROC: Number of MPI processes the program should run on
* [NODE]: First optional argument that defines the starting node of the graph (starting from 0)
* [DIM]: Second optional argument that defines the dimension/number of nodes a random undirected graph should have

The default starting node will be zero. If no dimension is given, the following test graph will be used.

![Image: Test graph](test_graph.png "Test graph")

Graphs can be translated into a matrix form. The test graph above (see [create_graph.c](src/create_graph.c#L12)) translates to:

```c
unsigned tmp_graph[][5] = {{  0,   4,   2, INF, INF},
                           {INF,   0,   3,   2,   3},
                           {INF,   1,   0,   4,   5},
                           {INF, INF, INF,   0, INF},
                           {INF, INF, INF,   1,   0}};
```

## Output
The output for the test graph will then look like this:

```bash
$ mpirun -np 2 ./dijkstra
Create graph...
Find path...

0	4	2	inf	inf
inf	0	3	2	3
inf	1	0	4	5
inf	inf	inf	0	inf
inf	inf	inf	1	0

Node	Dist.	Path
0	0	start
1	3	1 <- 2 <- start
2	2	2 <- start
3	5	3 <- 1 <- 2 <- start
4	6	4 <- 1 <- 2 <- start

Dimension:  5
Start node: 0
Processes:  2

Init.:      0.155248s
Dijkstra:   0.000052s
Total:      0.155437s
```
